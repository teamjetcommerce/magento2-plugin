<?php

namespace Richpanel\Analytics\Helper;

class ShipmentSerializer extends \Magento\Framework\App\Helper\AbstractHelper
{
    public function __construct(
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Richpanel\Analytics\Helper\ImagePathResolver $imagePathResolver,
        \Richpanel\Analytics\Helper\OrderSerializer $orderSerializer
    ) {
        $this->productRepository = $productRepository;
        $this->imagePathResolver = $imagePathResolver;
        $this->orderSerializer = $orderSerializer;
    }

    /**
     * Build individual order data
     *
     * @param  \Magento\Sales\Model\Order $order
     * @return array
     */
    public function buildShipmentForSubmission($shipment) {
        $identityData = $this->orderSerializer->orderIdentityData($shipment->getOrder());

        $call = array(
            'event'         => 'fulfillment',
            'properties'    => $this->prepareShipmentDetails($shipment),
            'userProperties'    => $identityData,
            'time' => array('sentAt' => round(microtime(true) * 1000))
        );

        ksort($call);
        return $call;
    }

    /**
     * Get order details and sort them for richpanel
     *
     * @param  Mage_Sales_Model_Order $order
     * @return array
     */
    public function prepareShipmentDetails($shipment) {
        $data = [
            'id'        => $shipment->getIncrementId(),
            'orderId'   => $shipment->getOrder()->getIncrementId(),
            'status'    => $shipment->getShipmentStatus(),
            'quantity'  => $shipment->getTotalQty(),
            'weight'    => $shipment->getTotalWeight()
        ];

        $createdAt = $shipment->getCreatedAt();
        if (!empty($createdAt)) {
            $date = strtotime($createdAt) * 1000;
            $data['createdAt'] = $date;
        }

        $updatedAt = $shipment->getUpdatedAt();
        if (!empty($updatedAt)) {
            $date = strtotime($updatedAt) * 1000;
            $data['updatedAt'] = $date;
        }

        // $data['comments'] = $shipment->getComments();
        $data['tracking'] = $this->getTrackingNumberByOrderData($shipment);
        $this->orderSerializer->assignBillingInfo($data, $shipment);
        
        $count = 0;
        $itemCollection = $shipment->getItemsCollection();
        foreach ($shipment->getItemsCollection() as $item) {
            $data['items'][] = $this->getProductDetails($item->getOrderItem());
            $data['items'][$count]['quantity'] = $item->getQty();
            $count++;
        }

        return $data;
    }

    public function getTrackingNumberByOrderData($shipment) {
        $trackNumbers = [];
        $tracksCollection = $shipment->getTracksCollection();
        if (!empty($tracksCollection)) {
            foreach ($tracksCollection->getItems() as $track) {
                $data = $track->getData();

                $result = array(
                    'trackingNumber' => $track->getTrackNumber(),
                );

                $company = $data["title"];
                if (empty($company)) {
                    $company = $data["carrier_code"];
                }

                $result['trackingCompany'] = $company;

                if (!empty($data["updated_at"])) {
                    $date = strtotime($data["updated_at"]) * 1000;
                    $result['shippingDate'] = $date;
                }

                $carrier_code = $track->getCarrierCode();
                $url = '';
                if ($carrier_code == 'usps') {
                    $url = 'https://tools.usps.com/go/TrackConfirmAction.action?tLabels=' . $track->getTrackNumber();
                } else if ($carrier_code == 'fedex'){
                    $url = 'https://www.fedex.com/fedextrack/?tracknumbers=' . $track->getTrackNumber();
                } else if ($carrier_code == 'ups'){
                    $url = 'http://wwwapps.ups.com/WebTracking/processInputRequest?TypeOfInquiryNumber=T&InquiryNumber1=' . $track->getTrackNumber();
                }

                if (!empty($url)) {
                    $result['trackingUrl'] = $url;
                }

                $trackNumbers[] = $result;
            }
        }
        return $trackNumbers;
    }

    private function getProductDetails($quoteItem) {
        $dataItem = array(
            'id'        => (int)$quoteItem->getProductId(),
            'price'     => (float)$quoteItem->getBasePrice(),
            'name'      => $quoteItem->getName(),
            'quantity'  => (int)$quoteItem->getQtyOrdered()
        );

        if ($quoteItem->getProductType() == 'configurable') {
            $options = (array)$quoteItem->getProductOptions();
            $dataItem['option_id'] = $options['simple_sku'];
            // for legacy reasons - we have been passing the SKU as ID for the child products
            $dataItem['option_sku'] = $options['simple_sku'];
            $dataItem['option_name'] = $options['simple_name'];
            $dataItem['option_price'] = (float)$quoteItem->getBasePrice();
        }

        try {
            if ($quoteItem->getProductType() == 'configurable') {
                $parentId = $quoteItem->getProductId();
                $product = $this->productRepository->getById($parentId);
            } else {
                $product = $quoteItem->getProduct();
            }

            if ($product) {
                $imageBasePath = $this->imagePathResolver->getBaseImage($product);
                if(!empty($imageBasePath)) {
                    $dataItem['image_url'] = array($imageBasePath);
                }
                $dataItem['url'] = $product->getProductUrl();
                $dataItem['sku'] = $product->getSku();

                if(count($product->getCategoryIds())) {
                    $categories = array();
                    $collection = $product->getCategoryCollection()
                        ->addAttributeToSelect('id')
                        ->addAttributeToSelect('name');
    
                    foreach ($collection as $category) {
                        $categories[] = array(
                            'id' => $category->getId(),
                            'name' => $category->getName()
                        );
                    }
                    $dataItem['categories'] = $categories;
                }
            }

        } catch (\Exception $e) {}

        return $dataItem;
    }
}
