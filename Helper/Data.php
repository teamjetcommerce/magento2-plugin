<?php

namespace Richpanel\Analytics\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Helper class
 *
 * @author Shubhanshu Chouhan <shubhanshu@richpanel.com>
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const DATA_TAG = 'richpanel_events';

    const MODULE_NAME = 'Richpanel_Analytics';

    public $js_domain = 'api.richpanel.com/v2';
    private $push_domain = 'https://api.richpanel.com/v2';
    
    protected $_customerFactory;
    protected $_addressFactory;
    protected $_customerSession;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Customer\Model\Session                    $session
     * @param \Psr\Log\LoggerInterface                           $logger
     * @param \Magento\Framework\Json\Helper\Data                $jsonHelper
     * @param Client                                             $clientHelper
     * @param OrderSerializer                                    $orderSerializer
     * @param \Magento\Store\Model\StoreManagerInterface         $storeManager
     * @param \Magento\Framework\App\ProductMetadata             $metaData
     * @param \Magento\Framework\Module\ModuleListInterface      $moduleList
     */
    public function __construct(
        // \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Customer\Model\Session $session,
        // \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Richpanel\Analytics\Helper\Client $clientHelper,
        \Richpanel\Analytics\Helper\OrderSerializer $orderSerializer,
        \Richpanel\Analytics\Helper\ShipmentSerializer $shipmentSerializer,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\ProductMetadata $metaData,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Customer\Model\SessionFactory $customerSessionFactory,
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\Http\Context $authContext
        // \Magento\Customer\Api\CustomerRepositoryInterfaceFactory $customerRepositoryFactory
        // \Magento\Framework\ObjectManagerInterface $objectmanager
    ) {
        // $this->config = $config;
        $this->session = $session;
        // $this->logger = $logger;
        $this->jsonHelper = $jsonHelper;
        $this->clientHelper = $clientHelper;
        $this->orderSerializer = $orderSerializer;
        $this->shipmentSerializer = $shipmentSerializer;
        $this->storeManager = $storeManager;
        $this->metaData = $metaData;
        $this->moduleList = $moduleList;
        // $this->objectManager = $objectmanager;
        $this->_customerFactory = $customerFactory->create();
        // $this->_customerFactory = $customerFactory;
        $this->_addressFactory = $addressFactory->create();
        // $this->_customerSession = $customerSession->create();
        $this->_customerSession = $customerSession;
        $this->authContext = $authContext;
        $this->customerSessionFactory = $customerSessionFactory->create();
        parent::__construct($context);   
    }

    public function updateWithUserDetails($customerId = NULL){

        $this->log('Calling updateWithUserDetails - ' . $customerId);

        // if ($this->_customerSession->isLoggedIn()) {
        //     $this->log('updateWithUserDetails - LoggedIn');
        //     $this->log($this->_customerSession->getId());
        //     $this->log($this->_customerSession->getCustomerData());
        // }

        // return NULL;
        
        $data = NULL;
        
        try {
            
            $customer = NULL;
            if($customerId){
                $this->log('updateWithUserDetails Fetching from customer id');
                $this->log($customerId);
                $customer = $this->_customerFactory->load($customerId);
            }
            else if ($this->_customerSession->isLoggedIn()) {
                $this->log('updateWithUserDetails Fetching from session');
                $this->log($this->_customerSession->getId());
                $customer = $this->_customerFactory->load($this->_customerSession->getId());
            } else if ($this->authContext->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH)) {
                $this->log('updateWithUserDetails Fetching from http');
                $this->log($this->customerSessionFactory->getCustomer()->getId());
                $customer = $this->_customerFactory->load($this->customerSessionFactory->getCustomer()->getId());
            }else {
                $this->log('notlogged in');
            }

            if($customer){
                // $this->log('updateWithUserDetails Fetching customer data');
                $user_properties = $customer->getData();

                $data = array(
                    'email' => $customer->getEmail(), 
                    'name'  => $customer->getName(),
                    'firstName' => $user_properties['firstname'], 
                    'lastName' => $user_properties['lastname'],
                    'dob' => $user_properties['dob'],
                    'uid'   => $customer->getEmail(),
                    'sourceId'   => (int)$customer->getId()
                );

                // $this->log('updateWithUserDetails Showing customer data');
                // $this->log($data);
                // $this->log($customer->getData());
                
                $customerBillingAddressId = $customer->getDefaultBilling();
                if($customerBillingAddressId){
                    $address = $this->_addressFactory->load($customerBillingAddressId);
                    $addressData = $address->getData();
                    $data['billingAddress'] = array(
                        'firstName'	=> $addressData['firstname'],
                        'lastName'	=> $addressData['lastname'],
                        'city'	=> $addressData['city'],
                        'state'	=> $addressData['region'],
                        'country'	=> $addressData['country_id'],
                        'postcode'	=> $addressData['postcode'],
                        'phone'	=> $addressData['telephone'],
                        'address1'	=> $addressData['street']
                    );
                }
        
                $customerShippingAddressId = $customer->getDefaultShipping();
                if($customerShippingAddressId){
                    $address = $this->_addressFactory->load($customerShippingAddressId);
                    $addressData = $address->getData();
                    $data['shippingAddress'] = array(
                        'firstName'	=> $addressData['firstname'],
                        'lastName'	=> $addressData['lastname'],
                        'city'	=> $addressData['city'],
                        'state'	=> $addressData['region'],
                        'country'	=> $addressData['country_id'],
                        'postcode'	=> $addressData['postcode'],
                        'phone'	=> $addressData['telephone'],
                        'address1'	=> $addressData['street']
                    );
                }

            }

        } catch (\Exception $e) {
            $this->log('updateWithUserDetails Error');
            $this->logError($e);
        }
        // $this->log('updateWithUserDetails Return');
        // $this->log($data);
        return $data;
    }

    /**
     * Get storeId for the current request context
     *
     * @param null $request
     *
     * @return int
     */
    public function getStoreId()
    {
        return $this->storeManager->getStore()->getId();
    }

    /**
     * Check if richpanel module is enabled
     *
     * @return boolean
     */
    public function isEnabled($storeId)
    {
        // $this->log('Calling isEnabled');
        return $this->scopeConfig->getValue(
            'richpanel_analytics/general/enable',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get API Token from system configuration
     *
     * @return string
     */
    public function getApiToken($storeId)
    {
        return $this->scopeConfig->getValue(
            'richpanel_analytics/general/api_key',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get API Secret from system configuration
     *
     * @return string
     */
    public function getApiSecret($storeId)
    {
        return $this->scopeConfig->getValue(
            'richpanel_analytics/general/api_secret',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get duration selected
     *
     * @return string
     */
    public function getDurationSelected($storeId)
    {
        // $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT;
        return $this->scopeConfig->getValue(
            'richpanel_analytics/general/rp_duration',
            // $scope,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get session data with "richpanel_events" key
     *
     * @return array
     */
    public function getSessionEvents()
    {
        $events = [];
        if ($this->session->getData(self::DATA_TAG)) {
            $events = $this->session->getData(self::DATA_TAG, true);
        }
        return $events;
    }

    /**
     * Add event to session
     *
     * @param string  $method
     * @param string  $type
     * @param array   $data
     * @param boolean|string $metaData
     */
    public function addSessionEvent($method, $type, $data = false, $userProperties = false, $customerId = false)
    {
        $this->log('Customer addSessionEvent');
        $events = [];
        if ($this->session->getData(self::DATA_TAG) != '') {
            $events = (array)$this->session->getData(self::DATA_TAG);
        }

        if ($customerId) {
            $userProperties = $this->updateWithUserDetails($customerId);
        } else {
            $tempData = $this->updateWithUserDetails();
            if ($tempData) {
                $userProperties = $tempData;
            }
        }

        $eventToAdd = array(
            'method' => $method,
            'type' => $type,
            'properties' => $data,
            'userProperties' => $userProperties
        );

        // if ($customerId) {
        //     $userProperties = $this->updateWithUserDetails($customerId);
        // } else {
        //     $tempData = $this->updateWithUserDetails();
        //     if ($tempData) {
        //         $userProperties = $tempData;
        //     }
        // }

        array_push($events, $eventToAdd);
        $this->session->setData(self::DATA_TAG, $events);
    }

    /**
     * API call to Richpanel to submit information
     *
     * @param  int $storeId
     * @param  array $orders
     * @return void
     */
    public function callBatchApi($storeId, $orders)
    {
        $this->log('Calling callBatchApi');
        $ordersForSubmission = $this->_buildOrdersForSubmission($orders);
        $call = $this->_buildCall($storeId, $ordersForSubmission);
        $this->_callRichpanelApi($storeId, $call);
    }

    public function callBatchApiForShipment($storeId, $shipments)
    {
        $this->log('Calling callBatchApiForShipment');
        try {
            $shipmentForSubmission = [];
            foreach ($shipments as $shipment) {
                $this->log($shipment->getIncrementId());
                if ($shipment->getIncrementId()) {
                    // array_push($shipmentForSubmission, $this->shipmentSerializer->buildShipmentForSubmission($shipment));
                    array_push($shipmentForSubmission, $this->orderSerializer->buildOrderForSubmission($shipment->getOrder(), $shipment));
                }
            }
    
            $call = $this->_buildCall($storeId, $shipmentForSubmission);
            $this->_callRichpanelApi($storeId, $call);
        } catch (\Exception $e) {
            $this->logError($e);
        }
    }

    /**
     * Create submition ready arrays from Array of \Magento\Sales\Model\Order
     *
     * @param \Magento\Sales\Model\Order[] $orders
     * @return array
     */
    protected function _buildOrdersForSubmission($orders)
    {
        $this->log('Calling _buildOrdersForSubmission');
        $ordersForSubmission = [];
        foreach ($orders as $order) {
            // $this->log($order->getId());
            if ($order->getId() && trim($order->getCustomerEmail())) {
                array_push($ordersForSubmission, $this->orderSerializer->buildOrderForSubmission($order));

                $deleteEvent = $this->orderSerializer->buildDeleteEvent($order);
                if (!empty($deleteEvent)) {
                    array_push($ordersForSubmission, $deleteEvent);
                }
            }
        }
        return $ordersForSubmission;
    }

    /**
     * Create call array
     *
     * @param  int $storeId
     * @param  array $ordersForSubmission
     * @return array
     */
    protected function _buildCall($storeId, $ordersForSubmission)
    {
        $this->log('Calling _buildCall');
        return array(
            'appClientId'    => $this->getApiToken($storeId),
            'events'   => $ordersForSubmission,
            // for debugging/support purposes
            'platform' => 'Magento ' . $this->metaData->getEdition() . ' ' . $this->metaData->getVersion(),
            'version'  => $this->moduleList->getOne(self::MODULE_NAME)['setup_version'],
            'event' => 'send_batch'
        );
    }

    /**
     * Submit orders to Richpanel API via post request
     *
     * @param  int $storeId
     * @param  array $call
     * @return void
     */
    protected function _callRichpanelApi($storeId, $call)
    {
        $this->log('Calling _callRichpanelApi');
        ksort($call);
        $basedCall = base64_encode($this->jsonHelper->jsonEncode($call));
        $signature = hash('sha256', $basedCall . $this->getApiSecret($storeId)); //md5($basedCall . $this->getApiSecret($storeId));
        $requestBody = [
            's'   => $signature,
            'hs'  => $basedCall
        ];
        $this->clientHelper->post($this->push_domain . '/bt', $requestBody);
    }

    public function log($value)
    {
        try {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/richpanel.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
    
            // if ($isError) {
            //     $logger->error($value);
            // } else {
            // }
            $logger->info($value);
        } catch (\Exception $e) {
            $this->logError($e);
        }
    }

    /**
     * Creates project activity
     *
     * @param string $type The type of the activity to create
     *
     * @return boolean Indicates if the creation was successful
     */
    public function createActivity($storeId, $type)
    {
        $key = $this->getApiToken($storeId);
        $secret = $this->getApiSecret($storeId);

        $data = array(
            'type' => $type,
            'signature' => hash('sha256', $key . $type . $secret) //md5($key . $type . $secret)
        );

        // $url = $this->push_domain.'/tracking/' . $key . '/activity';

        // $responseCode = $this->clientHelper->post($url, $data)['code'];
        $responseCode = 200;

        return $responseCode == 200;
    }

    /**
     * Log error to logs
     *
     * @param  \Exception $exception
     * @return void
     */
    public function logError($exception)
    {
        if ($exception instanceof \Exception) {
            $this->log($exception->getMessage());
        } else {
            $this->log($exception);
        }
    }
}
