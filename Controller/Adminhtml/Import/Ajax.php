<?php

namespace Richpanel\Analytics\Controller\Adminhtml\Import;

/**
 * AJAX Controller for sending chunks to Richpanel
 *
 * @author Shubhanshu Chouhan <shubhanshu@richpanel.com>
 */
class Ajax extends \Magento\Backend\App\Action
{
    /**
     * @param \Magento\Backend\App\Action\Context              $context
     * @param \Richpanel\Analytics\Helper\Data                   $helper
     * @param \Richpanel\Analytics\Model\Import                  $import
     * @param \Magento\Framework\App\Request\Http              $request
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
     # TODO: Ask Miro why \Magento\Framework|App\Action|Context won't compile
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Richpanel\Analytics\Helper\Data $helper,
        \Richpanel\Analytics\Model\Import $import,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        parent::__construct($context);
        $this->helper = $helper;
        $this->import = $import;
        $this->request = $request;
        $this->resultJsonFactory = $resultJsonFactory;
    }

    /**
     * Import orders history by chunks
     *
     * @throws \Exception
     * @return string
     */
    public function execute()
    {
        try {
            $jsonFactory = $this->resultJsonFactory->create();
            $result = ['success' => false];

            $storeId = (int)$this->request->getParam('storeId');
            $chunkId = (int)$this->request->getParam('chunkId');
            $duration = $this->request->getParam('duration') || '';
            // $totalChunks = (int)$this->request->getParam('totalChunks');

            // if ($chunkId == 0) {
            //     $this->helper->createActivity($storeId, 'import_start');
            // }

            // Get orders from the Database
            $orders = $this->import->getOrders($storeId, $chunkId, $duration);
            // Send orders via API helper method
            $this->helper->callBatchApi($storeId, $orders, false);
            $result['success'] = true;

            // if ($chunkId == $totalChunks - 1) {
            //     $this->helper->createActivity($storeId, 'import_end');
            // }

            return $jsonFactory->setData($result);
        } catch (\Exception $e) {
            return $jsonFactory->setData([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
}
