/**
 * Add import js file to
 * Require JS mapping
 */
var config = {
    map: {
        '*': {
            richpanelImport: 'Richpanel_Analytics/js/import'
        }
    },
    deps: [
        "jquery"
    ]
};