<?php

namespace Richpanel\Analytics\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CustomerLogin implements ObserverInterface
{

    /**
     * @param \Richpanel\Analytics\Helper\Data $helper
     */
    public function __construct(
        \Richpanel\Analytics\Helper\Data $helper
    ) {
        $this->helper = $helper;
    }

    /**
     * Track customer login and trigger "identify" to Richpanel
     *
     * @param  \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        try {
            $this->helper->log('Customer Login Event');
            // $customer = $observer->getEvent()->getCustomer();
            $customer = $observer->getData('customer');
            // $this->helper->log($customer);
            if (empty($customer) || !$customer) {
                return;
            }

            $data = [
                'uid'       => $customer->getEmail(),
                'email'     => $customer->getEmail(),
                'name'          => $customer->getName(),
                'firstName'    => $customer->getFirstname(),
                'lastName'     => $customer->getLastname(),
            ];
            $this->helper->addSessionEvent('identify', 'identify', false, $data, $customer->getId());

        } catch (\Exception $e) {
            $this->helper->logError($e);
        }
    }
}
