<?php

namespace Richpanel\Analytics\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class Shipment implements ObserverInterface
{
    private $helper;

    /**
     * @param \Richpanel\Analytics\Helper\Data $helper
     */
    public function __construct(
        \Richpanel\Analytics\Helper\Data $helper
    ) {
        $this->helper = $helper;
    }

     /**
     * Trigger on save Shipment
     *
     * @param  \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        try {
            $this->helper->log('Shipment Event');
            $shipment = $observer->getEvent()->getShipment();

            $storeId = $shipment->getStoreId();

            if (!$this->helper->isEnabled($storeId)) {
                $this->helper->log('Shipment - Store disabled');
                return;
            }

            foreach ($shipment->getItemsCollection() as $item) {    
                $id = $item->getOrderItem()->getProductId();    
            }

            $this->helper->callBatchApiForShipment($storeId, [$shipment]);

        } catch (\Exception $e) {
            $this->helper->logError($e);
        }
    }
}