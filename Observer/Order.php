<?php

namespace Richpanel\Analytics\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class Order implements ObserverInterface
{

    private $helper;

    /**
     * @param \Richpanel\Analytics\Helper\Data $helper
     */
    public function __construct(
        \Richpanel\Analytics\Helper\Data $helper
    ) {
        $this->helper = $helper;
    }

    /**
     * Trigger on save Order
     *
     * @param  \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        try {
            $this->helper->log('Order Event');
            $order = $observer->getEvent()->getOrder();
            // $order = $observer->getData('order');
            
            $storeId = $order->getStoreId();

            if (!$this->helper->isEnabled($storeId)) {
                $this->helper->log('Order - Store disabled');
                return;
            }

            $this->helper->callBatchApi($storeId, [$order]);

            // If order is made from the FrontEnd
            if ($order->getRemoteIp() && trim($order->getCustomerEmail())) {
                $this->helper->log('Order - Identify');
                $data = array(
                    'uid'       => $order->getCustomerEmail(),
                    'email'      => $order->getCustomerEmail(),
                    'name'       => $order->getBillingAddress()->getName(),
                    'firstName' => $order->getBillingAddress()->getFirstname(),
                    'lastName'  => $order->getBillingAddress()->getLastname()
                );
                $this->helper->addSessionEvent('identify', 'identify', false, $data);
            }
        } catch (\Exception $e) {
            $this->helper->logError($e);
        }
    }
}
