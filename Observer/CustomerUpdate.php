<?php

namespace Richpanel\Analytics\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CustomerUpdate implements ObserverInterface
{

    /**
     * @param \Richpanel\Analytics\Helper\Data                    $helper
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     */
    public function __construct(
        \Richpanel\Analytics\Helper\Data $helper,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
    ) {
        $this->helper = $helper;
        $this->customerRepository = $customerRepository;
    }

    /**
     * Track customer update profile information
     * and trigger "identify" to Richpanel
     *
     * @param  \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        try {
            $this->helper->log('Customer Update Event');
            $email = $observer->getEvent()->getEmail();
            if (empty($email) || !$email) {
                return;
            }

            $customer = $this->customerRepository->get($email);
            if ($customer) {
                $data = [
                    'uid'       => $customer->getEmail(),
                    'email'     => $customer->getEmail()
                ];
                
                if (method_exists($customer, 'getName')) {
                    $data['name'] = $customer->getName();
                }
                if (method_exists($customer, 'getFirstname')) {
                    $data['firstName'] = $customer->getFirstname();
                }
                if (method_exists($customer, 'getLastname')) {
                    $data['lastName'] = $customer->getLastname();
                }
                $this->helper->addSessionEvent('identify', 'identify', false, $data, $customer->getId());
            }
        } catch (\Exception $e) {
            $this->helper->logError($e);
        }
    }
}
