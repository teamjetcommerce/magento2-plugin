<?php

namespace Richpanel\Analytics\Model\Config\Source;

class ListMode implements \Magento\Framework\Option\ArrayInterface
{
 public function toOptionArray()
 {
  return [
    ['value' => 'resume', 'label' => __('Resume Last Sync')],
    ['value' => '-1 Months', 'label' => __('Last 30 days')],
    ['value' => '-2 Months', 'label' => __('Last 60 days')],
    ['value' => '-3 Months', 'label' => __('Last 3 months')],
    ['value' => '-6 Months', 'label' => __('Last 6 months')],
    ['value' => '-12 Months', 'label' => __('Last 12 months')]
  ];
 }
}