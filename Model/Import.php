<?php
/**
 * @author Shubhanshu Chouhan <shubhanshu@richpanel.com>
 */

namespace Richpanel\Analytics\Model;

/**
 * Model getting orders by chunks for Richpanel import
 *
 * @author Shubhanshu Chouhan <shubhanshu@richpanel.com>
 */
class Import
{
    private $ordersTotal = 0;
    private $totalChunks = 0;
    private $chunkItems  = 50;
    private $data;
    // private $scopeConfig;

    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollection,
        \Richpanel\Analytics\Helper\AdminStoreResolver $resolver,
        \Richpanel\Analytics\Helper\Data $data
        // \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->orderCollection = $orderCollection;
        $this->resolver = $resolver;
        $this->data = $data;
        // $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return int
     */
    public function getTotalChunks()
    {
        return $this->totalChunks;
    }

    /**
     * Get chunk orders
     *
     * @param  int
     * @return
     */
    public function getOrders($storeId, $chunkId, $duration)
    {
        return $this->getOrderQuery($storeId, $duration)
            ->setPageSize($this->chunkItems)
            ->setCurPage($chunkId + 1);
    }

    /**
     * Chunks array
     *
     * @return int
     */
    public function getChunks($storeId = 0, $duration = '-12 Months')
    {
        $storeTotal = $this->getOrderQuery($storeId, $duration)->getSize();
        return (int) ceil($storeTotal / $this->chunkItems);
    }

    /**
     * Get contextual store id
     *
     * @return int
     */
    public function getStoreId()
    {
        return (int) $this->resolver->getAdminStoreId();
    }

    /**
     * @param int $storeId
     *
     * @return mixed
     */
    protected function getOrderQuery($storeId = 0, $duration = '-12 Months')
    {
        $durationSelected = $this->getDurationSelected($storeId);
        if ($durationSelected !== 'resume') {
            $dateMinusDuration = date("Y-m-d h:i:s",strtotime($durationSelected));
        } else {
            $dateMinusDuration = date("Y-m-d h:i:s",strtotime($duration));
        }

        return $this->orderCollection->create()
        ->addAttributeToFilter('store_id', $storeId)
        ->addAttributeToFilter('updated_at', ['gteq' => $dateMinusDuration])
        ->setOrder(
            'created_at',
            'desc'
        );
    }

    public function getDurationSelected($storeId)
    {
        $selectedOption = $this->data->getDurationSelected($storeId);
        if ($selectedOption == null) {
            return '';
        } 
        return $selectedOption;
    }
}
