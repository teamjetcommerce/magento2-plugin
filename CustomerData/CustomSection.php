<?php
namespace Richpanel\Analytics\CustomerData;

use Magento\Customer\CustomerData\SectionSourceInterface;

/**
 * Catalog Product Compare Widget
 */
class CustomSection implements SectionSourceInterface
{
    /**
     * @var \Richpanel\Analytics\Block\Analytics
     */
    protected $analytics;

    /**
     * @param \Richpanel\Analytics\Block\Analytics $analytics
     */
    public function __construct(
        \Richpanel\Analytics\Block\Analytics $analytics
    ) {
        $this->analytics = $analytics;
    }

    /**
     * Customer Data
     */
    public function getSectionData()
    {
        return $this->analytics->getRichpanelUserData();
    }
}