<?php

namespace Richpanel\Analytics\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Block rendering events to frontend
 *
 * @author Shubhanshu Chouhan <shubhanshu@richpanel.com>
 */
class Analytics extends Template
{

    /**
     * @var \Richpanel\Analytics\Helper\Data
     */
    public $helper;

    /**
     * @param Context                            $context
     * @param \Richpanel\Analytics\Helper\Data     $helper
     * @param \Richpanel\Analytics\Model\Analytics $dataModel
     * @param \Magento\Customer\Model\Session    $session
     * @param array                              $data
     */
    public function __construct(
        Context $context,
        \Richpanel\Analytics\Helper\Data $helper,
        \Richpanel\Analytics\Model\Analytics $dataModel,
        \Magento\Customer\Model\Session $session,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->dataModel = $dataModel;
        $this->_session = $session;
        parent::__construct($context, $data);
    }

    /**
     * Get API Token
     *
     * @return bool|null|string
     */
    public function getApiToken()
    {
        return $this->helper->getApiToken($this->helper->getStoreId());
    }

    /**
     * Get events to track them to Richpanel js api
     *
     * @return array
     */
    public function getEvents()
    {
        return array_merge(
            $this->helper->getSessionEvents(),
            $this->dataModel->getEvents()
        );
    }

    public function getRichpanelUserData() {
        $data = $this->helper->updateWithUserDetails();
        if ($data != NULL) {
            return array(
                'data' => $this->encryptData($data, $this->helper->getStoreId()),
                'normalData' => $data
            );
        } else {
            return array('data' => NULL, 'normalData' => NULL);
        }
    }

    public function encryptData($data, $storeId = NULL){

        $api_secret = $this->helper->getApiSecret($storeId);

        $method = 'AES-256-CBC';
        $key = hash('sha256', $api_secret);
        
        return $this->encrypt(json_encode($data), $key, $method);
    }

    public function encrypt($data, $key, $method) {
        $iv_size        = openssl_cipher_iv_length($method);
        $iv             = openssl_random_pseudo_bytes($iv_size);
        $ciphertext     = openssl_encrypt($data, $method, $key, OPENSSL_RAW_DATA, $iv);
        $ciphertext_hex = bin2hex($ciphertext);
        $iv_hex         = bin2hex($iv);
        return "$iv_hex:$ciphertext_hex";
    }

    /**
     * Render richpanel js if module is enabled
     *
     * @return string
     * @codeCoverageIgnore
     */
    protected function _toHtml()
    {
        if (!$this->helper->isEnabled($this->helper->getStoreId())) {
            return '';
        }
        return parent::_toHtml();
    }
}
