<?php

namespace Richpanel\Analytics\Cron;

class OrderUpdateCheck
{

	public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Richpanel\Analytics\Helper\Data $helper,
        \Richpanel\Analytics\Model\Import $import
    ) {
        $this->helper = $helper;
        $this->import = $import;
        $this->storeManager = $storeManager;
    }

	public function execute()
	{
		$this->sync('-5 minutes');
	}

	public function executeDaily()
	{
		$this->sync('-24 hours');
	}

	public function sync($duration = '-5 minutes') {
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/cron.log');
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		$logger->info(__METHOD__);

		// $duration = '-5 minutes';
		$storeManagerDataList = $this->storeManager->getStores();

		foreach ($storeManagerDataList as $key => $value) {
			$storeId = $key;
			$chunks = $this->import->getChunks($storeId, $duration);
			foreach (range(0,$chunks) as $chunkId) {
				$orders = $this->import->getOrders($storeId, $chunkId, $duration);
				$this->helper->callBatchApi($storeId, $orders, false);
			}
		}
		return $this;
	}
}